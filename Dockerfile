FROM python:3
WORKDIR /app
COPY . .
RUN pip3 install -r requirenments.txt

EXPOSE 5000

CMD ["flask", "run"]
